package com.example.edoofun;

public class Users {
    String gambar, deskripsi,username;

    public Users(String gambar, String deskripsi, String username) {
        this.gambar = gambar;
        this.deskripsi = deskripsi;
        this.username = username;
    }

    public Users() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
