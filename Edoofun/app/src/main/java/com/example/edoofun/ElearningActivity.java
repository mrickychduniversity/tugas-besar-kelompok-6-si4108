package com.example.edoofun;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class ElearningActivity extends AppCompatActivity {

    Button btnTentang, btnVideo, btnQuiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elearning);

        loadFragment(new ElearningTentang());

        btnTentang = findViewById(R.id.button);
        btnVideo = findViewById(R.id.button4);
        btnQuiz = findViewById(R.id.button5);

        btnTentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new ElearningTentang());
            }
        });

        btnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new ElearningVideo());
            }
        });

        btnQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new ElearningQuiz());
            }
        });



    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout2, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
