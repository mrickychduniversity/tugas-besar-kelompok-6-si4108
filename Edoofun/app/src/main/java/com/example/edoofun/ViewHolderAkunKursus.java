package com.example.edoofun;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolderAkunKursus extends RecyclerView.ViewHolder {

    CardView cv;
    TextView title;
    TextView writer;
    ImageView imageView;

    ViewHolderAkunKursus(View itemView) {
        super(itemView);
        cv = itemView.findViewById(R.id.cardview);
        title = itemView.findViewById(R.id.username);
        writer = itemView.findViewById(R.id.writer);
        imageView = itemView.findViewById(R.id.imageView5);
    }
}
