package com.example.edoofun;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.Random;
import java.util.UUID;


public class GrupmancingActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 234;
    FirebaseAuth mAuth;
    private String identifier;
    private String uid;
    private ImageView ivPhoto;
    private EditText etStatus;
    private Uri filePath;
    private DatabaseReference databaseReference;

    private LinearLayout linContent;
    private LinearLayout linJoin;
    private String status;

    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupmancing);

        Button btnAnnouncement = findViewById(R.id.btnAnnouncement);
        btnAnnouncement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toAnnouncement();
            }
        });

        Button btnPhotos = findViewById(R.id.btnPhotos);
        btnPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toPhotos();
            }
        });

        ImageView ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageView ivMore = findViewById(R.id.ivMore);
        ivMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KomunitasMancingActivity();
            }
        });

        LinearLayout linPhoto = findViewById(R.id.linPhoto);
        linPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        ivPhoto = findViewById(R.id.ivPhoto);

        ImageView ivSubmit = findViewById(R.id.ivSubmit);
        ivSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile();
            }
        });

        LinearLayout linVideo = findViewById(R.id.linVideo);
        linVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        etStatus = findViewById(R.id.etStatus);
        linContent = findViewById(R.id.linContent);
        linJoin = findViewById(R.id.linJoin);

    }

    public void toAnnouncement() {
        Intent intent = new Intent(this, AnnouncementActivity.class);
        intent.putExtra("name", "Oki Sabeni");
        intent.putExtra("judul", "Rules Komunitas Memancing");
        startActivity(intent);
    }

    private void toPhotos() {
        Intent intent = new Intent(this, PhotosActivity.class);
        startActivity(intent);
    }

    private void KomunitasMancingActivity() {
        Intent intent = new Intent(this, KomunitasMancingActivity.class);
        intent.putExtra("judul", "Komunitas Memancing");
        intent.putExtra("image", R.mipmap.minat_memancing_foreground);
        startActivity(intent);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ivPhoto.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //this method will upload the file
    private void uploadFile() {
        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getUid();
        identifier = mAuth.getCurrentUser().getEmail();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference current_user_db = database.getReference(uid);
        String status = etStatus.getText().toString();

        String ID = UUID.randomUUID().toString();
        Random rand = new Random();

        int aduh = rand.nextInt(1000000);
        final String aidi = String.valueOf(aduh);
        //if there is a file to upload
        if (filePath != null) {
            //displaying a progress dialog while upload is going on
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading");
            progressDialog.show();
            final StorageReference riversRef = FirebaseStorage.getInstance().getReference().child("images/" + ID + ".jpg");

            riversRef.putFile(filePath)

                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //if the upload is successfull
                            //hiding the progress dialog
                            progressDialog.dismiss();

                            //and displaying a success toast
                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                            riversRef.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    String profileImageUrl = task.getResult().toString();
                                    current_user_db.child(aidi).child("gambar").setValue(profileImageUrl);
                                    Log.i("URL", profileImageUrl);
                                }
                            });


                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //hiding the progress dialog
                            progressDialog.dismiss();

                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                            //displaying percentage in progress dialog
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });

            current_user_db.child(aidi).child("username").setValue(identifier);
        }

        if (!status.equals("")) {
            current_user_db.child(aidi).child("status").setValue(status);
            Toast.makeText(this, "Success update status", Toast.LENGTH_SHORT).show();
            etStatus.setText("");
        }

        //if there is not any file
        else {
            //you can display an error toast
        }
    }
}
