package com.example.edoofun;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class postingan_detail extends AppCompatActivity {
    TextView tDeskripsi, tUsername, tUsernamebawah;
    ImageView tGambar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.postingan_detail);
        Intent intent = getIntent();
        tUsernamebawah = findViewById(R.id.txt_nama_atas);
        tDeskripsi = findViewById(R.id.txt_deskripsi);
        tUsername = findViewById(R.id.txt_nama_bawah);
        tGambar = findViewById(R.id.view_gambarr);

        String usernamebwh = intent.getStringExtra("username");
        String username = intent.getStringExtra("username");
        String deskripsi = intent.getStringExtra("deskripsi");
        String gambar = intent.getStringExtra(sosial.gambar);

        tUsernamebawah.setText(usernamebwh);
        tDeskripsi.setText(deskripsi);
        tUsername.setText(username);
        Picasso.with(postingan_detail.this).load(gambar).into(tGambar);
    }

    public void back(View view) {
        finish();
    }
}
