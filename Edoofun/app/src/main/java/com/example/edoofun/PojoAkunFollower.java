package com.example.edoofun;

public class PojoAkunFollower {

    public String username;
    public int imageId;

    public PojoAkunFollower(String username, int imageId) {

        this.username = username;
        this.imageId = imageId;
    }

    public PojoAkunFollower () {

    }

    public int getImageId(){return imageId;}
    public void setImageId(int imageId){
        this.imageId=imageId;
    }

    public String getUsername(){return username;}
    public void setWriter(String writer){
        this.username=username;
    }

}
