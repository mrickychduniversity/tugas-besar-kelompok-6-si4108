package com.example.edoofun;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolderAkunFollowing extends RecyclerView.ViewHolder {

    CardView cv;
    TextView username;
    ImageView imageView;

    ViewHolderAkunFollowing(View itemView) {
        super(itemView);
        cv = itemView.findViewById(R.id.cardview);
        username = itemView.findViewById(R.id.username);
        imageView = itemView.findViewById(R.id.imageView5);
    }
}
