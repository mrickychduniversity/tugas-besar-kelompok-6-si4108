package com.example.edoofun;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

public class sosial  extends AppCompatActivity {

    private RecyclerView mResultList;
    private DatabaseReference mDatabaseReference;
    FirebaseRecyclerAdapter adapter;
    public static final String gambar = "gaje" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sosial);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("Users");
        mResultList =  findViewById(R.id.recycler);
        BottomNavigationView bottomNavigationView = findViewById(R.id.btn_nav);

        bottomNavigationView.setSelectedItemId(R.id.sosial_menu);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        return true;

                    case R.id.keranjang_menu:
                        startActivity(new Intent(getApplicationContext(),
                                PembayaranKoin.class));
                        overridePendingTransition(0, 0);
                        return true;

                    case R.id.sosial_menu:
                        startActivity(new Intent(getApplicationContext(),
                                sosial.class));
                        overridePendingTransition(0, 0);
                        return true;

                    case R.id.account_menu:
                        startActivity(new Intent(getApplicationContext(),
                                AkunActivity.class));
                        overridePendingTransition(0, 0);

                        return true;
                }
                return false;

            }

        });
        String text = "Richard";
        firebaseUserView(text);


    }



    public void LaunchDM(View view) {
        Intent intent = new Intent(this, inbox.class);
        startActivity(intent);
    }

    public void LaunchPostBaru(View view) {
        Intent intent = new Intent(this, postingan_baru.class);
        startActivity(intent);
    }

    public void LaunchPostDetail(View view) {
        Intent intent = new Intent(this, postingan_detail.class);
        startActivity(intent);

    }

    public void LaunchMinat(View view) {
        Intent intent = new Intent(this, pilihan_minat.class);
        startActivity(intent);
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;
        ImageView gambar;
        TextView txtDeskripsi;
        public UsersViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            gambar = (ImageView) mView.findViewById(R.id.view_gambar);
            txtDeskripsi = (TextView) mView.findViewById(R.id.textview_deskripsi);

        }
    }

    private void firebaseUserView(String searchText) {

        Query query = mDatabaseReference.orderByChild("username").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerOptions<Users> options =
                new FirebaseRecyclerOptions.Builder<Users>()
                        .setQuery(mDatabaseReference, Users.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<Users, UsersViewHolder>(options) {
            @NonNull
            @Override
            public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewgambarsosial, parent, false);
                UsersViewHolder viewHolder = new UsersViewHolder(view);
                return viewHolder;
            }

            @Override
            protected void onBindViewHolder(@NonNull UsersViewHolder holder, int i, @NonNull Users model) {
                Picasso.with(sosial.this).load(model.getGambar()).into(holder.gambar);
                holder.txtDeskripsi.setText(model.getUsername());
                final String nDeskripsi = model.getDeskripsi();
                final String nUsername = model.getUsername();
                final String mGambar = model.getGambar();
                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    Intent intent = new Intent(sosial.this, postingan_detail.class);
                    intent.putExtra("deskripsi",nDeskripsi);
                    intent.putExtra("username",nUsername);
                    intent.putExtra(gambar,mGambar);
                    startActivity(intent);
                    }
                });
            }
        };
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this);
        mResultList.setLayoutManager(gridLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mResultList.getContext(),
                gridLayoutManager.getOrientation());
        mResultList.addItemDecoration(dividerItemDecoration);
        adapter.startListening();
        mResultList.setAdapter(adapter);
    }


}
