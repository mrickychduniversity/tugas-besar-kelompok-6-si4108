package com.example.edoofun;

public class PojoAkunKursus {

    private int id;

    public String title;
    public String writer;
    public int imageId;

    public PojoAkunKursus(String title, String writer, int imageId) {
        this.title = title;
        this.writer = writer;
        this.imageId = imageId;
    }

    public PojoAkunKursus () {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImageId(){return imageId;}
    public void setImageId(int imageId){
        this.imageId=imageId;
    }

    public  String getTitle(){return title;}
    public void setTitle(String title){
        this.title=title;
    }

    public String getWriter(){return writer;}
    public void setWriter(String writer){
        this.writer=writer;
    }

}
