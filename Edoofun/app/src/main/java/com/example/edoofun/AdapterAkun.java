package com.example.edoofun;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

public class AdapterAkun extends RecyclerView.Adapter<ViewHolderAkunKursus> {

    List<PojoAkunKursus> list = Collections.emptyList();
    Context context;

    public AdapterAkun(List<PojoAkunKursus> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolderAkunKursus onCreateViewHolder(final ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_kursus, parent, false);
        final ViewHolderAkunKursus holder = new ViewHolderAkunKursus(v);

        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(parent.getContext(), "Test Click" + String.valueOf(holder.getAdapterPosition()), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(parent.getContext(), ElearningActivity.class);
                parent.getContext().startActivity(intent);
            }
        });
        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolderAkunKursus holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.title.setText(list.get(position).title);
        holder.writer.setText(list.get(position).writer);
        holder.imageView.setImageResource(list.get(position).imageId);


        //animate(holder);

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, PojoAkunKursus PojoAkunKursus) {
        list.add(position, PojoAkunKursus);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified PojoAkunKursus object
    public void remove(PojoAkunKursus PojoAkunKursus) {
        int position = list.indexOf(PojoAkunKursus);
        list.remove(position);
        notifyItemRemoved(position);
    }


}