package com.example.edoofun;

public class PojoAkunFollowing {

    public String username;
    public int imageId;

    public PojoAkunFollowing(String username, int imageId) {

        this.username = username;
        this.imageId = imageId;
    }

    public PojoAkunFollowing () {

    }

    public int getImageId(){return imageId;}
    public void setImageId(int imageId){
        this.imageId=imageId;
    }

    public String getUsername(){return username;}
    public void setWriter(String writer){
        this.username=username;
    }

}
