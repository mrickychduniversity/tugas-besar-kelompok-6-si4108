package com.example.edoofun;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

public class AdapterAkunPosting extends RecyclerView.Adapter<ViewHolderAkunPosting> {

    List<PojoAkunPosting> list = Collections.emptyList();
    Context context;

    public AdapterAkunPosting(List<PojoAkunPosting> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolderAkunPosting onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_posting, parent, false);
        ViewHolderAkunPosting holder = new ViewHolderAkunPosting(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolderAkunPosting holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView

        holder.imageView.setImageResource(list.get(position).imageId);

        //animate(holder);

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, PojoAkunPosting PojoAkunPosting) {
        list.add(position, PojoAkunPosting);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified PojoAkunPosting object
    public void remove(PojoAkunPosting PojoAkunPosting) {
        int position = list.indexOf(PojoAkunPosting);
        list.remove(position);
        notifyItemRemoved(position);
    }

}