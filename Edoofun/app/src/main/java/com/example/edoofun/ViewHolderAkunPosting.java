package com.example.edoofun;

import android.view.View;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolderAkunPosting extends RecyclerView.ViewHolder {

    CardView cv;
    ImageView imageView;

    ViewHolderAkunPosting(View itemView) {
        super(itemView);
        cv = itemView.findViewById(R.id.cardview);
        imageView = itemView.findViewById(R.id.imageView5);
    }
}
