package com.example.edoofun;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

public class AdapterAkunFollower extends RecyclerView.Adapter<ViewHolderAkunFollower> {

    List<PojoAkunFollower> list = Collections.emptyList();
    Context context;

    public AdapterAkunFollower(List<PojoAkunFollower> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolderAkunFollower onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_follower, parent, false);
        ViewHolderAkunFollower holder = new ViewHolderAkunFollower(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolderAkunFollower holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView

        holder.username.setText(list.get(position).username);
        holder.imageView.setImageResource(list.get(position).imageId);

        //animate(holder);

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, PojoAkunFollower PojoAkunFollower) {
        list.add(position, PojoAkunFollower);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified PojoAkunFollower object
    public void remove(PojoAkunFollower PojoAkunFollower) {
        int position = list.indexOf(PojoAkunFollower);
        list.remove(position);
        notifyItemRemoved(position);
    }

}